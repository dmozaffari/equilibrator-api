"""unit test for PhaseReaction and ComponentContribution."""
# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import warnings

import numpy
import pytest

from equilibrator_api import (
    Q_,
    ComponentContribution,
    Reaction,
    ccache,
    parse_reaction_formula,
)
from equilibrator_api.phased_compound import GAS_PHASE_NAME
from helpers import approx_unit


@pytest.fixture(scope="module")
def comp_contribution() -> ComponentContribution:
    """Create a ComponentContribution object."""
    return ComponentContribution(p_h=Q_("7"), ionic_strength=Q_("0.25M"))


@pytest.fixture(scope="module")
def atp_hydrolysis() -> Reaction:
    """Create a ATP hydrolysis reaction."""
    formula = "kegg:C00002 + kegg:C00001 = kegg:C00008 + kegg:C00009"
    return parse_reaction_formula(formula)


@pytest.fixture(scope="module")
def missing_h2o() -> Reaction:
    """Create a ATP hydrolysis reaction."""
    formula = "kegg:C00002 = kegg:C00008 + kegg:C00009"
    return parse_reaction_formula(formula)


@pytest.fixture(scope="module")
def fermentation_gas() -> Reaction:
    """Create a reaction object of glucose => 2 ethanol + 2 CO2(g)."""
    formula = "kegg:C00031 = 2 kegg:C00469 + 2 kegg:C00011"
    rxn = parse_reaction_formula(formula)
    rxn.set_phase(ccache.get_compound("kegg:C00011"), GAS_PHASE_NAME)
    return rxn


def test_water_balancing(comp_contribution, missing_h2o):
    """Test if the reaction can be balanced using H2O."""
    assert not missing_h2o.is_balanced()
    assert missing_h2o.is_balanced(ignore_atoms=("H", "O", "e-"))

    balanced_rxn = missing_h2o.balance_with_compound(
        ccache.get_compound("kegg:C00001"), ignore_atoms=("H",)
    )
    assert balanced_rxn is not None


def test_atp_hydrolysis_physiological_dg(comp_contribution, atp_hydrolysis):
    """Test the dG adjustments for physiological conditions (with H2O)."""
    warnings.simplefilter("ignore", ResourceWarning)
    assert atp_hydrolysis.is_balanced()
    assert float(atp_hydrolysis.physiological_dg_correction()) == pytest.approx(
        numpy.log(1e-3), rel=1e-3
    )

    atp_hydrolysis.set_abundance(ccache.get_compound("kegg:C00002"), Q_("1uM"))
    atp_hydrolysis.set_abundance(ccache.get_compound("kegg:C00008"), Q_("1uM"))
    atp_hydrolysis.set_abundance(ccache.get_compound("kegg:C00009"), Q_("1uM"))

    assert float(atp_hydrolysis.dg_correction()) == pytest.approx(
        numpy.log(1e-6), rel=1e-3
    )


def test_fermentation_gas(comp_contribution, fermentation_gas):
    """Test the dG adjustments for physiological conditions (in gas phase)."""
    assert fermentation_gas.is_balanced()
    assert float(
        fermentation_gas.physiological_dg_correction()
    ) == pytest.approx(numpy.log(1e-9), rel=1e-3)


def test_atp_hydrolysis_dg(comp_contribution, atp_hydrolysis):
    """Test the CC predictions for ATP hydrolysis."""
    warnings.simplefilter("ignore", ResourceWarning)

    atp_hydrolysis.set_abundance(ccache.get_compound("kegg:C00002"), Q_("1mM"))
    atp_hydrolysis.set_abundance(ccache.get_compound("kegg:C00008"), Q_("10mM"))
    atp_hydrolysis.set_abundance(ccache.get_compound("kegg:C00009"), Q_("10mM"))

    standard_dg_prime = comp_contribution.standard_dg_prime(atp_hydrolysis)

    approx_unit(standard_dg_prime.value, Q_("-25.8 kJ/mol"), abs=0.1)
    approx_unit(standard_dg_prime.error, Q_("0.3 kJ/mol"), abs=0.1)

    dg_prime = comp_contribution.dg_prime(atp_hydrolysis)
    approx_unit(dg_prime.value, Q_("-31.5 kJ/mol"), abs=0.1)

    physiological_dg_prime = comp_contribution.physiological_dg_prime(
        atp_hydrolysis
    )
    approx_unit(physiological_dg_prime.value, Q_("-42.9 kJ/mol"), abs=0.1)


def test_gibbs_energy_pyruvate_decarboxylase(comp_contribution):
    """Test the CC predictions for pyruvate decarboxylase."""
    warnings.simplefilter("ignore", ResourceWarning)

    # pyruvate = acetaldehyde + CO2
    formula = "kegg:C00022 = kegg:C00084 + kegg:C00011"
    reaction = parse_reaction_formula(formula)

    assert reaction.is_balanced()

    standard_dg_prime = comp_contribution.standard_dg_prime(reaction)
    approx_unit(standard_dg_prime.value, Q_("-18.0 kJ/mol"), abs=0.1)
    approx_unit(standard_dg_prime.error, Q_("3.3 kJ/mol"), abs=0.1)


def test_reduction_potential(comp_contribution):
    """Test the CC predictions for a redox half-reaction."""
    warnings.simplefilter("ignore", ResourceWarning)

    # oxaloacetate = malate
    formula = "kegg:C00036 = kegg:C00149"
    reaction = parse_reaction_formula(formula)

    assert reaction.check_half_reaction_balancing() == 2
    assert reaction.is_balanced(ignore_atoms=("H", "e-"))

    standard_e_prime = comp_contribution.standard_e_prime(reaction)

    approx_unit(standard_e_prime.value, Q_("-177.0 mV"), abs=1.0)
    approx_unit(standard_e_prime.error, Q_("3.3 mV"), abs=1.0)


def test_unresolved_reactions(comp_contribution):
    """Test the CC predictions for a reaction that cannot be resolved."""
    formula = (
        "kegg:C09844 + kegg:C00003 + kegg:C00001 => "
        "kegg:C03092 + kegg:C00004"
    )

    reaction = parse_reaction_formula(formula)
    standard_dg_prime = comp_contribution.standard_dg_prime(reaction)
    assert standard_dg_prime.error.magnitude > 1e4


def test_reversibility_index(comp_contribution, atp_hydrolysis):
    """Test the reversibility index."""

    ln_gamma = comp_contribution.ln_reversibility_index(atp_hydrolysis)
    approx_unit(ln_gamma.value, Q_("-11.54"), abs=0.1)
