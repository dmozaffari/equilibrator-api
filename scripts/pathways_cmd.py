"""Command-line script for pathway analysis."""
# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import argparse
import logging

from matplotlib.backends.backend_pdf import PdfPages

from equilibrator_api import Pathway


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description='Calculate the Max-min Driving Force (MDF) of a pathway.')
    parser.add_argument(
        'infile', type=argparse.FileType('r'),
        help='path to input file containing reactions')
    parser.add_argument(
        'outfile', type=argparse.FileType('wb'),
        help='path to output PDF file')
    logging.getLogger().setLevel(logging.WARNING)

    args = parser.parse_args()

    pp = Pathway.from_sbtab(args.infile)

    mdf_res = pp.calc_mdf()

    with PdfPages(args.outfile) as pdf:
        pdf.savefig(mdf_res.compound_plot)
        pdf.savefig(mdf_res.reaction_plot)

    net_rxn = pp.net_reaction()
    rxn_df = mdf_res.reaction_df
    cpd_df = mdf_res.compound_df
